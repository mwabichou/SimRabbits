#ifndef lib
#define lib
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
typedef struct Rabbit
{
    char gender;
    int age;
    int isPregnant;
    int isMature;
    int isAlive;
    float survival_rate;
    int maturity_age;
    int litters_per_year;
    int next_litter;
    struct Rabbit *next;

} Rabbit;

#define SIZE_BUFF 512
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */
static unsigned long mt[N];     /* the array for the state vector  */
static int mti = N + 1;         /* mti==N+1 means mt[N] is not initialized */

void init_genrand(unsigned long s);
void init_by_array(unsigned long init_key[], int key_length);
unsigned long genrand_int32(void);
long genrand_int31(void);
double genrand_real1(void);
double genrand_real2(void);
double genrand_real3(void);
double genrand_res53(void);
Rabbit *create_rabbit(int isMature);
Rabbit *initialize_rabbits(int num_rabbits, float matureRatio);
void update_rabbits(Rabbit *head);
int run_simulation(Rabbit *head, int num_rabbits, int months);
void free_rabbits(Rabbit *head);
void confidence_interval(int nbSim, int Months, float matureRatio);
void fiboSimu(int nbSteps);
void menu();
#endif /*lib*/