#include "lib.h"

#define NUM_RABBITS 10
#define SIMULATION_MONTHS 365
#define MATURITY_AGE_MIN 5
#define MATURITY_AGE_MAX 8
#define LITTERS_PER_YEAR_MIN 4
#define LITTERS_PER_YEAR_MAX 8
#define KITTENS_PER_LITTER_MIN 3
#define KITTENS_PER_LITTER_MAX 6
#define ADULT_SURVIVAL_RATE 0.6
#define LITTLE_SURVIVAL_RATE 0.35
#define OLD_AGE 120
#define MAX_AGE 180

/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/* Nom : create_rabbit                                                                            */
/*                                                                                                */
/* Entrées : un int isMature (mature ou non à la creation)                                        */
/*                                                                                                */
/* Sorties : un pointeur sur Rabbit                                                               */
/*                                                                                                */
/* Description : Cette fonction crée un lapin et l'initialise avec des valeurs aléatoires         */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

Rabbit *create_rabbit(int isMature)
{
    Rabbit *new_rabbit = (Rabbit *)malloc(sizeof(Rabbit));

    new_rabbit->gender = (genrand_real1() < 0.5) ? 'M' : 'F';
    new_rabbit->isPregnant = 0;
    new_rabbit->isMature = isMature;
    new_rabbit->isAlive = 1;
    new_rabbit->maturity_age = MATURITY_AGE_MIN + genrand_int32() % (MATURITY_AGE_MAX - MATURITY_AGE_MIN + 1);
    new_rabbit->litters_per_year = LITTERS_PER_YEAR_MIN + genrand_int32() % (LITTERS_PER_YEAR_MAX - LITTERS_PER_YEAR_MIN + 1);
    new_rabbit->next_litter = 0;

    if (isMature)
    {
        new_rabbit->survival_rate = ADULT_SURVIVAL_RATE;
        new_rabbit->age = new_rabbit->maturity_age;
    }
    else
    {
        new_rabbit->survival_rate = LITTLE_SURVIVAL_RATE;
        new_rabbit->age = 0;
    }
    new_rabbit->next = NULL;

    return new_rabbit;
}

/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/* Nom : initialize_rabbits                                                                       */
/*                                                                                                */
/* Entrées : un int num_rabbits (nombre de lapins à créer)                                        */
/*           un float matureRatio (ratio de lapins matures à la création)                         */
/*                                                                                                */
/* Sorties : un pointeur sur Rabbit                                                               */
/*                                                                                                */
/* Description : Cette fonction crée un nombre de lapins donné et les initialise avec des valeurs */
/*               aléatoires                                                                       */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

Rabbit *initialize_rabbits(int num_rabbits, float matureRatio)
{
    if (num_rabbits <= 0)
    {
        return NULL;
    }

    Rabbit *head = create_rabbit(genrand_real1() < matureRatio);

    Rabbit *current = head;

    for (int i = 1; i < num_rabbits; i++)
    {
        if (genrand_real1() < matureRatio)
        {
            current->next = create_rabbit(1);
        }
        else
        {
            current->next = create_rabbit(0);
        }
        current = current->next;
    }
    return head;
}

/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/* Nom : couple                                                                                   */
/*                                                                                                */
/* Entrées : un pointeur sur Rabbit                                                               */
/*                                                                                                */
/* Sorties : un int (1 si il y a un couple, 0 sinon)                                              */
/*                                                                                                */
/* Description : Cette fonction vérifie si il y a un couple de lapins                             */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

int couple(Rabbit *head)
{
    Rabbit *current = head;

    while (current != NULL)
    {
        if (current->isAlive && current->gender == 'M' && current->isMature)
        {
            return 1;
        }
        current = current->next;
    }

    return 0;
}

/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/* Nom : update_rabbits                                                                           */
/*                                                                                                */
/* Entrées : un pointeur sur Rabbit                                                               */
/*                                                                                                */
/* Sorties : void                                                                                 */
/*                                                                                                */
/* Description : Cette fonction met à jour les lapins                                             */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

void update_rabbits(Rabbit *head)
{
    Rabbit *current = head;
    Rabbit *tail = NULL;

    while (current != NULL)
    {
        if (current->isAlive)
        {
            current->age++;

            if (current->age >= current->maturity_age && !current->isMature)
            {
                current->isMature = 1;
                current->survival_rate = ADULT_SURVIVAL_RATE;
            }
            if (current->age % 12 == 0)
            {
                if (genrand_real1() > current->survival_rate || current->age >= MAX_AGE)
                {
                    current->isAlive = 0;
                }
                if (current->age >= OLD_AGE)
                {
                    current->survival_rate -= 0.1;
                }
            }
            else if (!current->isMature && current->age == 1)
            {
                if (genrand_real1() > current->survival_rate)
                {
                    current->isAlive = 0;
                }
            }
            if (current->isPregnant)
            {
                int num_kittens = KITTENS_PER_LITTER_MIN + genrand_int32() % (KITTENS_PER_LITTER_MAX - KITTENS_PER_LITTER_MIN + 1);
                current->isPregnant = 0;

                for (int i = 0; i < num_kittens; i++)
                {
                    Rabbit *new_rabbit = create_rabbit(0);
                    new_rabbit->next = current->next;
                    current->next = new_rabbit;
                }
            }
            if (current->gender == 'F' && current->isMature && !current->isPregnant && couple(head))
            {
                if (current->next_litter == 0)
                {
                    current->next_litter = 12 / current->litters_per_year;
                }
                else
                {
                    current->next_litter--;
                }

                if (current->next_litter == 0)
                {
                    current->isPregnant = 1;
                }
            }
            tail = current;
        }
        else
        {
            if (tail != NULL)
            {
                tail->next = current->next;
                free(current);
                current = tail;
            }
        }
        current = current->next;
    }
}

/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/* Nom : run_simulation                                                                           */
/*                                                                                                */
/* Entrées : un pointeur sur Rabbit                                                               */
/*           un int num_rabbits (nombre de lapins à créer)                                        */
/*           un int months (nombre de mois à simuler)                                             */
/*                                                                                                */
/* Sorties : un int (nombre de lapins vivants à la fin de la simulation)                          */
/*                                                                                                */
/* Description : Cette fonction simule la vie des lapins pendant un nombre de mois donné          */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

int run_simulation(Rabbit *head, int num_rabbits, int months)
{
    int alive_count, mature_count, dead_count, pregnant_count, male_count, female_count;
    for (int month = 0; month < months; month++)
    {
        alive_count = 0;
        mature_count = 0;
        dead_count = 0;
        pregnant_count = 0;
        male_count = 0;
        female_count = 0;

        update_rabbits(head);
        Rabbit *current = head;
        while (current != NULL)
        {
            if (current->isAlive)
            {
                alive_count++;

                if (current->isMature)
                {
                    mature_count++;
                }
                if (current->isPregnant)
                {
                    pregnant_count++;
                }
                if (current->gender == 'M')
                {
                    male_count++;
                }
                if (current->gender == 'F')
                {
                    female_count++;
                }
            }
            else
            {
                dead_count++;
            }
            current = current->next;
        }
        // blue color
        printf("-------------------MONTH N.%d | YEAR N.%d------------------------\n\n", month + 1, (month + 1) / 12);
        printf("Males Alive : %d\t\t\t", male_count);
        printf(" Females Alive : %d\n\n", female_count);
        printf("Pregnant : %d\t\t\t", pregnant_count);
        printf(" Mature : %d\n\n", mature_count);
        printf("Total Death : \033[0;31m%d\033[0m\t\t\t", dead_count);
        printf(" Total Alive : \033[0;32m%d\033[0m\n\n", alive_count);
    }
    return alive_count;
}
/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/* Nom : free_rabbits                                                                             */
/*                                                                                                */
/* Entrées : un pointeur sur Rabbit                                                               */
/*                                                                                                */
/* Sorties : void                                                                                 */
/*                                                                                                */
/* Description : Cette fonction libère la mémoire allouée pour les lapins                         */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

void free_rabbits(Rabbit *head)
{
    Rabbit *current = head;
    Rabbit *next;

    while (current != NULL)
    {
        next = current->next;
        free(current);
        current = next;
    }
}

/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/* Nom : fiboSimu                                                                                 */
/*                                                                                                */
/* Entrées : nbSteps : nombre de mois à simuler                                                   */
/*                                                                                                */
/* Sorties : void                                                                                 */
/*                                                                                                */
/* Description : Simulation de la croissance de la population de lapins                           */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

void fiboSimu(int nbSteps)
{
    int prevCouple = 0, currentCouple = 1, nextCouple;

    for (int i = 0; i < nbSteps; i++)
    {
        nextCouple = prevCouple + currentCouple;
        prevCouple = currentCouple;
        currentCouple = nextCouple;
        printf(" Month %d: %d couples\n", i + 1, currentCouple);
    }
}

/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/* Nom : confidence_interval                                                                      */
/*                                                                                                */
/* Entrées : nbSim : nombre de simulations                                                        */
/*           Months : nombre de mois                                                              */
/*           matureRatio : ratio de maturité                                                      */
/*                                                                                                */
/* Sorties : void                                                                                 */
/*                                                                                                */
/* Description : Calcul de l'intervalle de confiance                                              */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

void confidence_interval(int nbSim, int Months, float matureRatio)
{
    double *rbTab = (double *)malloc(nbSim * sizeof(double)), tTab[30] = {63.657, 9.925, 5.841, 4.604, 4.032, 3.707, 3.499, 3.355, 3.25, 3.169, 3.106, 3.055, 3.012, 2.977, 2.947, 2.921, 2.898, 2.878, 2.861, 2.845, 2.831, 2.819, 2.807, 2.797, 2.787, 2.779, 2.771, 2.763, 2.756, 2.75};
    double meanAlive = 0, t = 0, variance = 0, stdDev = 0, sum;
    int i = 0, j = 0, nbAlive = 0;

    for (i = 0; i < nbSim; i++)
    {
        printf("Simulation %d/%d \n", i + 1, nbSim);
        Rabbit *head = initialize_rabbits(NUM_RABBITS, matureRatio);
        nbAlive = run_simulation(head, NUM_RABBITS, Months);
        free_rabbits(head);
        meanAlive += nbAlive;
        rbTab[i] = nbAlive;
    }

    meanAlive /= nbSim;

    sum = 0;
    for (i = 0; i < nbSim; i++)
    {
        sum += (rbTab[i] - meanAlive) * (rbTab[i] - meanAlive);
    }

    variance = sum / (nbSim - 1);

    stdDev = sqrt(variance);

    if (nbSim < 30)
    {
        t = tTab[nbSim - 1];
    }
    else
    {
        t = 2.567;
    }
    double interval = t * stdDev / sqrt(nbSim);
    printf("Mean: %f , StdDev: %f , Interval: %f t: %f \n", meanAlive, stdDev, interval, t);
    printf("Confidence Interval: [%f, %f]\n", meanAlive - interval, meanAlive + interval); // 95% confidence interval
    free(rbTab);
}
/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/* Nom : menu                                                                                     */
/*                                                                                                */
/* Entrées : void                                                                                 */
/*                                                                                                */
/* Sorties : void                                                                                 */
/*                                                                                                */
/* Description : Menu de l'application                                                            */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

void menu()
{
    int choix, months, nbRabbits, nbSim, nbPoints, nbSteps;
    float matureRatio;
    system("clear");
    do
    {
        printf("\t\t------------------MENU------------------ \n");
        printf("\t\t1. Fibonacci Simulation\n");
        printf("\t\t2. More Relistic Rabbit Simulation\n");
        printf("\t\t3. Confidence Interval\n");
        printf("\t\t0. Quitter \n");
        printf("Votre choix: ->");
        scanf("%d", &choix);
        switch (choix)
        {
        case 1:
            fflush(stdin);
            printf("Entrer the number of steps: ");
            scanf("%d", &nbSteps);
            printf("    \n");
            fiboSimu(nbSteps);
            printf("    \n");
            break;
        case 2:
            fflush(stdin);
            printf("Entrer the number of months: ");
            scanf("%d", &months);
            printf("    \n");
            printf("Entrer the number of rabbits: ");
            scanf("%d", &nbRabbits);
            printf("    \n");
            printf("Entrer the mature ratio: ");
            scanf("%f", &matureRatio);
            printf("    \n");
            Rabbit *head = initialize_rabbits(nbRabbits, matureRatio);
            run_simulation(head, nbRabbits, months);
            free_rabbits(head);
            printf("    \n");
            break;
        case 3:
            fflush(stdin);
            printf("Entrer the number of simulations: ");
            scanf("%d", &nbSim);
            printf("    \n");
            printf("Entrer the number of months: ");
            scanf("%d", &months);
            printf("    \n");
            printf("Entrer the mature ratio: ");
            scanf("%f", &matureRatio);
            printf("    \n");
            confidence_interval(nbSim, months, matureRatio);
            printf("    \n");
            break;
        case 0:
            printf("Goodbye \n");
            break;
        default:
            printf("Choix invalide \n");
            break;
        }
    } while (choix != 0);
}

/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/* Nom : main                                                                                     */
/*                                                                                                */
/* Entrées : void                                                                                 */
/*                                                                                                */
/* Sorties : void                                                                                 */
/*                                                                                                */
/* Description : Fonction principale                                                              */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/

int main()
{
    init_genrand(time(NULL));
    menu();
    return 0;
}
